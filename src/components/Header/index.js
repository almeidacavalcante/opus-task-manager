import React from 'react'
import { MdShortText } from "react-icons/md";

import { Container } from "./styles";
import Icon from '../Icon';
import theme from '../../styles/theme';

export default function Header() {
    return (
        <Container>
            <Icon color={theme.dark} size={'35px'}>
                <MdShortText />
            </Icon>
        </Container>
    )
}
