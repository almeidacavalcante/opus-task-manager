import styled from 'styled-components';
import theme from '../../styles/theme';

export const Container = styled.div`
    display: flex;
    padding: 30px;
    height: 100vh;
    background: ${theme.lighter};
`;
