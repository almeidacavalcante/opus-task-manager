import styled from 'styled-components';
import theme from '../../styles/theme';

export const Container = styled.div`
    position: ${props => props.toBottom ? 'absolute' : 'relative'};
    bottom: ${props => props.toBottom ? '0px' : 'initial'};
    color: ${theme.light};
    font-size: 32px;
    opacity: 0.7;
    cursor: pointer;
    transition: all 0.3s ease;
    margin-top: 25px;
    margin-bottom: 25px;

    &:hover {
        color: #fff;
        opacity: 1;
    }

    & svg {
        color: ${props => props.color};
        font-size: ${props => props.size};
        margin: 0 auto;
    }
`;
