import { createGlobalStyle } from 'styled-components'
import theme from './theme'

export default createGlobalStyle`
    @import url('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap');

    * {
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
    }

    html, body, #root {
        height: 100%;
    }

    body {
        font: 14px 'Poppins', sans-serif;
        background: ${theme.light};
        color: #444;
        -webkit-font-smoothing: antialiased !important;
    }

    ul {
        list-style: none;
    }
`