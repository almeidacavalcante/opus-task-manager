import React, { useRef, useContext } from 'react';

import { Container, Label } from './styles';
import { useDrag, useDrop } from "react-dnd";
import BoardContext from "../Board/context";

export default function Card({ data, index, listIndex, dimmed }) {
    const ref = useRef();
    const { move } = useContext(BoardContext);

    const [{ isDragging }, dragRef] = useDrag({
        item: { type: 'CARD', index, listIndex, data },

        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),

        isDragging(monitor) {
            return data.id === monitor.getItem().data.id;
        }
    })

    const [, dropRef] = useDrop({
        accept: 'CARD',

        hover(item, monitor) {
            const draggedListIndex = item.listIndex;
            const targetListIndex = listIndex;
            const draggedIndex = item.index;
            const targetIndex = index;

            if (draggedIndex === targetIndex && draggedListIndex === targetListIndex) {
                return;
            }

            const targetSize = ref.current.getBoundingClientRect();
            const targetCenter = (targetSize.bottom - targetSize.top) / 2;

            const draggedOffset = monitor.getClientOffset();
            const draggedTop = draggedOffset.y - targetSize.top;

            if (draggedIndex < targetIndex && draggedTop < targetCenter) {
                return;
            }

            if (draggedIndex > targetIndex && draggedTop > targetCenter) {
                return;
            }

            move(draggedListIndex, targetListIndex, draggedIndex, targetIndex);
            item.index = targetIndex;
            item.listIndex = targetListIndex;
        },
    })

    dragRef(dropRef(ref))

    return (
        <Container
            ref={ref}
            isDragging={isDragging}
            dimmed={dimmed}
        >
            <header>
                {data.labels.map(label => <Label key={label} color={label}></Label>)}
            </header>
            <p>{data.content}</p>
            {data.users && data.users
                .map(u => <img key={u.profile} src={u.profile} style={{ background: u.bgColor }} alt='profile' />)}
        </Container>
    );
}
