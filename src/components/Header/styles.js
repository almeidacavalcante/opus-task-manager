import styled from 'styled-components';
import theme from '../../styles/theme';

export const Container = styled.div`
    height: 80px;
    padding: 0 40px;
    background: ${theme.silver};
    display: flex;
    align-items: center;
`;
