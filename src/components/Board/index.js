import React, { useState } from 'react';

import { Container } from './styles';
import List from '../List/index';
import { loadBoard } from '../../services/api';
import BoardContext from './context';
import clone from "lodash/cloneDeep";

const data = loadBoard();

export default function Board() {
    const [board, setBoard] = useState(data);

    const move = (fromList, toList, from, to) => {
        const tmpBoard = clone(board);
        const card = tmpBoard.lists[fromList].cards[from];
        tmpBoard.lists[fromList].cards.splice(from, 1);
        tmpBoard.lists[toList].cards.splice(to, 0, card);
        setBoard(tmpBoard);
    }

    const add = (card, toList) => {
        const tmpBoard = clone(board);
        tmpBoard.lists[toList].cards.unshift(card);
        setBoard(tmpBoard);
    }

    return (
        <BoardContext.Provider value={{ move, add }}>
            <Container>
                {board.lists.map((list, index) => <List key={list.title} index={index} data={list} />)}
            </Container>
        </BoardContext.Provider>
    );
}
