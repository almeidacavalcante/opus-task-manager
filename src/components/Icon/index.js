import React from 'react';

import { Container } from './styles';

export default function Icon({ children, color, size, toBottom, click }) {
    return (
        <Container onClick={click} color={color} size={size} toBottom={toBottom}>
            {children}
        </Container>
    );
}
