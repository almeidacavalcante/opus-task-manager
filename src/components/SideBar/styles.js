import styled from 'styled-components';
import theme from '../../styles/theme';

export const Container = styled.div`
    background: ${theme.green};
    width: ${props => props.collapsed ? '120px' : '180px'};
    position: relative;
    margin-right: -40px;
    padding: 0 40px 0 0;
    z-index: 1;
    height: 100vh;
    box-sizing: border-box;
    transition: all 0.3s ease-in-out;

    svg {
        position: relative;
        margin-left: ${props => props.collapsed ? '22px' : '48px'};
        transition: all 0.3s ease-in-out;
    }

    .collapse-icon {
        transition: all 0.3s ease-in-out;
        transform: ${props => props.collapsed ? 'rotateZ(180deg)' : 'rotateZ(0deg)'}
    }

    & .profile{
        
        & img {
            width: ${props => props.collapsed ? '45px' : ''};
            height:  ${props => props.collapsed ? '45px' : ''};
        }

        & .name {
            font-size: ${props => props.collapsed ? '12px' : '14px'};
            transition: all 0.3s ease-in-out;
        }

        & .at {
            opacity: ${props => props.collapsed ? 0 : 0.8};
            transition: all 0.3s ease-in-out;
        }

    }
`;
