import React from 'react';

import { Container } from './styles';

export default function Profile({ color, img, bgColor }) {
    return (
        <Container className='profile' color={color} bgColor={bgColor}>
            <img src={img} alt='profile' />
            <span className='name'>Mr. Jones</span>
            <span className='at'>@JonJones</span>
        </Container>
    );
}
