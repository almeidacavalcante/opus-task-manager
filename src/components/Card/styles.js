import styled, { css } from 'styled-components';
import theme from '../../styles/theme';

export const Container = styled.div`
    position: relative;
    background: ${theme.light};
    border-radius: 10px;
    margin-bottom: 10px;
    padding: 20px;
    box-shadow: 0px 1px 2px 0 rgba(192, 208, 230, 0.8);
    border-top: 5px solid rgba(230, 236, 245, 0.4);
    box-sizing: border-box;
    cursor: grab;
    color: #666;
    font-weight: 200;
    opacity: ${props => props.dimmed ? 0.6 : 1};

    header {
        position: absolute;
        top: -10px;
        left: 20px;
    }

    p {
        font-weight: 500;
        line-height: 20px;
    }

    img {
        width: 30px;
        height: 30px;
        border-radius: 5px;
        margin-top: 5px;
        padding: 2px;
        margin-right: 2px;
    }

    ${props => props.isDragging && css`
        border: 2px dashed rgba(0,0,0,0.3);
        background: ${theme.silver};
        opacity: 0.2;
        padding: 25px;
        border-radius: 5px;
        box-shadow: none;
        box-sizing: border-box;
        cursor: grabbing;

        p, img, header {
            opacity: 0;
        }
    `}
`;

export const Label = styled.span`
    width: 10px;
    height: 10px;
    border-radius: 2px;
    position: relative;
    background: ${ props => props.color};
`;
