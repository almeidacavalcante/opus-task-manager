import React, { useState } from "react";

import { Container } from "./styles";
import Icon from "../Icon";
import theme from "../../styles/theme";
import {
	MdInsertInvitation,
	MdInsertChart,
	MdDirectionsRun,
	MdDashboard
} from "react-icons/md";
import { FaAngleDoubleLeft } from "react-icons/fa";
import Profile from "../Profile/index";
import av2 from "../../assets/av2.png";

export default function SideBar() {
	const [isCollapsed, setIsCollapsed] = useState(false)

	const handleCollapse = (event) => {
		setIsCollapsed(!isCollapsed);
	};

	return (
		<Container collapsed={isCollapsed}>
			<Profile color={theme.light} bgColor={theme.yellow} img={av2} />
			<Icon color={theme.lighter} size={"28px"}>
				<MdDashboard />
			</Icon>
			<Icon color={theme.lighter} size={"28px"}>
				<MdInsertInvitation />
			</Icon>
			<Icon color={theme.lighter} size={"28px"}>
				<MdInsertChart />
			</Icon>
			<Icon color={theme.lighter} size={"28px"}>
				<MdDirectionsRun />
			</Icon>
			<Icon click={handleCollapse} color={theme.lighter} size={"28px"} toBottom={true}>
				<FaAngleDoubleLeft className='collapse-icon'  />
			</Icon>
		</Container>
	);
}
