const theme = {
    primary: '#66FCF1',
    secondary: '#45A29E',
    tertiary: '#C5C6C7',
    dark: '#1f2833',
    middark: '#131921',
    darker: '#0B0C10',
    light: '#eef1f7',
    lighter: '#f8f8f8',
    silver: '#eff3f9',

    blue: '#12b7ff',
    green: '#07B783',
    purple: '#6F52ED',
    yellow: '#ffd973',
    cream: '#ffedcc',
}

export default theme;