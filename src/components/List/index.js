import React, { useContext } from 'react';

import { Container } from './styles';
import { MdAdd } from 'react-icons/md';
import Card from '../Card/index';
import theme from '../../styles/theme';
import users from '../../services/api';
import BoardContext from '../Board/context';

export default function List({ data, index: listIndex }) {
    const { add } = useContext(BoardContext);

    const createCard = () => {
        const card = {
            id: Math.random(),
            content: 'Nova Tarefa criada através do botão criar.',
            labels: [theme.blue],
            users: [users[1], users[0], users[2]],
            isDragging: false
        }
        add(card, 0);
    }

    return (
        <Container yellow>
            <header>
                <h2>{data.title}</h2>
                {data.creatable && (
                    <button type='button' onClick={createCard}>
                        <MdAdd size={20} color={theme.dark}></MdAdd>
                    </button>
                )}

            </header>

            <ul>
                {data.cards.map((card, index) => (
                    <Card
                        key={card.id}
                        listIndex={listIndex}
                        index={index}
                        data={card}
                        dimmed={data.done}
                    />
                ))}
            </ul>
        </Container>
    );
}
