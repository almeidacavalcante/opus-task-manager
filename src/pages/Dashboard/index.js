import React from 'react';

import Board from '../../components/Board/index';

export default function Dashboard() {
    return (
        <>
            <Board />
        </>
    );
}
