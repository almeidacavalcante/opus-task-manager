import styled from 'styled-components';
import theme from '../../styles/theme';
import { darken } from 'polished';

export const Container = styled.div`
    padding: 0 20px;
    height: 100%;
    flex: 0 0 320px;

    & + div {
        border-left: 1px solid rgba(0,0,0,0.05);
    }

    header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            height: 42px;
            color: gray

        h2 {
            font-weight: 500;
            font-size: 16px;
            padding: 0 10px;
        }

        button {
            width: 40px;
            height: 40px;
            border-radius: 10px;
            background: ${theme.yellow};
            border: 0;
            cursor: pointer;
            transition: all 0.2s ease;

            &:hover {
                background-color: ${props => darken(0.2, theme.yellow)}
            }
        }
    }

    ul {
        margin-top: 30px;
    }
`;
