import React from 'react';
import { DndProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import GlobalStyle from "./styles/global";
import Header from './components/Header';
import Board from './components/Board';
import SideBar from './components/SideBar/index';
import Main from './components/Main/index';
import Dashboard from './pages/Dashboard';

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Main>
        <SideBar />

        <div style={{ width: '100%', overflow: 'hidden', borderRadius: '40px 0 0 40px', zIndex: 2 }}>
          <Header />
          <Dashboard />
        </div>
        <GlobalStyle />
      </Main>
    </DndProvider >
  );
}

export default App;
