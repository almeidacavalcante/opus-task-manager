import styled from 'styled-components';
import theme from '../../styles/theme';
import { darken } from 'polished';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 40px 0;
    position: relative;
    align-items: center;
    cursor: pointer;
    transition: all 0.3s ease;
    background: transparent;
    text-align: center;
    flex-wrap: wrap;
    transition: all 0.3s ease-in-out;

    &:hover > img {
        box-shadow: 0px 4px 8px 0px ${props => darken(0.07, theme.green)};
    }

    &:hover {
        transform: scale(1.05);
    }

    img {
        border-radius: 10px;
        width: 55px;
        height: 55px;
        padding: 1%;
        transition: all 0.2s ease;
        background: ${props => props.bgColor};
    }

    span {
        color: ${props => props.color};
    }

    .name {
        margin: 10px 0 0 0;
        font-weight: 700;
        font-size: 14px;
    }

    .at {
        font-size: 12px;
        font-weight: 500;
        opacity: 0.8;
    }
`;
