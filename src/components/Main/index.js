import React, { Children } from 'react';

import { Container } from './styles';

export default function Main(props) {
    return (
        <Container>
            {props.children}
        </Container>
    );
}
